from configparser import ConfigParser
import os, sys, getopt

class BuildProperties:
    def __init__(self, config):
        properties = ConfigParser()
        properties.read(os.path.join(os.getenv("HOME"), "pbt.properties"))
        self.config = config
        self.basedir = properties[config]['base.dir']
        self.mvn_opts = properties[config]['standard.maven.cli.options']
        self.modules = properties[config]['module.list'].split(',')
        self.vcs = properties[config]['vcs.update.command']
        self.vcs_root = properties[config]['vcs.root.url']
        self.vcs_checkout = properties[config]['vcs.checkout.command']

    def base_dir(self):
        return self.basedir

    def mvn_options(self):
        return self.mvn_opts

    def module_list(self):
        return self.modules

    def update_vcs(self):
        return self.vcs

    def clone_vcs(self):
        return self.vcs_checkout + " " + self.vcs_root


def do(op, in_dir, out_dir):
    print("\033[38;5;46m===> \033[38;5;209m{}\033[0m".format(in_dir))
    os.chdir(in_dir)
    print(op)
    os.system(op)
    os.chdir(out_dir)

def vcs_clone(props, modules):
    cwd = os.getcwd()
    os.chdir(props.base_dir())
    for m in modules:
        os.system(props.clone_vcs() + m)

def vcs_update(props, modules):
    cwd = os.getcwd()
    os.chdir(props.base_dir())
    for m in modules:
        do(props.update_vcs(), os.path.join('.', m), props.base_dir())
    os.chdir(cwd)

def run_mvn(props, modules, goals):
    cwd = os.getcwd()
    os.chdir(props.base_dir())
    if isinstance(goals, list):
        g = ' '.join(goals)
    else:
        g = goals
    mvn = 'mvn ' + props.mvn_options() + ' ' + g
    for m in modules:
        do(mvn, os.path.join('.', m), props.base_dir())
    os.chdir(cwd)

def usage():
    print("pbt config [options] mvn_goals")
    print("[options] : -c --checkout")
    print("[options] : -o --only <project>")
    print("[options] : -s --skip-build")
    print("[options] : -u --update-vcs")

def main():
    try:
        config = sys.argv[1]
        props = BuildProperties(config)
        modules = props.module_list()
        update_vcs = False
        skip_mvn = False
        clone_vcs = False

        opts, args = getopt.getopt(sys.argv[2:], "o:usc",
                                   ["only=",
                                    "skip-build",
                                    "update-vcs",
                                    "checkout"])

        goals = ' '.join(args)
        if not goals: # no goals provided -- skip_mvn implied
            skip_mvn = True

        for o, a in opts:
            if o in ("-c", "--checkout"):
                clone_vcs = True
            elif o in ("-o", "--only"):
                modules = [a]
            elif o in ("-s", "--skip-build"):
                skip_mvn = True
            elif o in ("-u", "--update-vcs"):
                update_vcs = True

        if clone_vcs:
            vcs_clone(props, modules)

        if update_vcs:
            vcs_update(props, modules)

        if not skip_mvn:
            run_mvn(props, modules, goals)

    except getopt.GetoptError as err:
        # print help information and exit
        print(err)
        print()
        usage()
        sys.exit(2)
    except Exception as err:
        print(err)
        print()
        usage()
        sys.exit(2)

if __name__ == "__main__":
    main()
