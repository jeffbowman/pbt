Python build tool
=================

## Abstract ##

Builds maven projects in various directories. Basically, I got tired
of writing a new shell (or other) script to build all the maven
'modules' for my project, when they were not already hierarchical. I
often have to start at a base directory, cd into one 'module', get an
updated version from vcs, run mvn, cd back to the parent and start the
process over again for the next 'module'.

Ideally, I could do this once, and with a simple configuration file,
establish the base directory, the module list in desired build order
and allow this tool to manage the tasks mentioned above.

## Properties ##

pbt.properties should reside your home directory with the following
contents

    [project1]
    base.dir =
    standard.maven.cli.options =
    module.list =
    vcs.update.command =

    [project2]
    base.dir =
    standard.maven.cli.options =
    module.list =
    vcs.update.command =

- *project1, project2, etc*  -- the name of the configuration this block applies to
- base.dir  -- the directory where modules live as subdirectories
- standard.maven.cli.options  -- anything that should be added to the mvn command line (this is not MVN_OPTS)
- module.list  -- the list of modules in build order
- vcs.update.command  -- update command use by version control, git pull, hg pull -u, etc.

## Command Line Options ##

python pbt.py [config name] [options - see below] [mvn goals]

* -o : --only       | only build the specified module
* -s : --skip-build | skip running maven, only useful with -u
* -u : --update-vcs | get an updated version from vcs

*config name* | the name of the configuration to use (as specified in the pbt.properites file)  
*mvn goals* | the maven goal(s) to run, **there are no default goals**

### Examples
    [project1]
    base.dir = Projects
    standard.maven.cli.options = -B -f master-pom.xml
    module.list = module1, module2, module3, module4
    vcs.update.command = git pull

    [project2]
    base.dir = workspace
    standard.maven.cli.options = 
    module.list = m1, m2, m3, m4
    vcs.update.command = hg pull -u


1. `python pbt.py project1 clean`  
turns into `mvn -B -f master-pom.xml clean` which will be run for each module
1. `python pbt.py project1 -us`  
turns into `git pull` for each module
1. `python pbt.py project1 -u clean test`  
runs `git pull` for each module then runs `mvn -B -f master-pom.xml clean test` for each module
1. `python pbt.py project1 -o module2 test -Dtest=SomeClassTest`  
turns into `mvn -B -f master-pom.xml test -Dtest=SomeClassTest` for only module2
1. `python pbt.py project2 -o m4 install`  
turns into `mvn install` for only the m4 module
1. `python pbt.py project2 -us`  
runs `hg pull -u` for each module in project2

## Future ##

- Create plugin system
- Fix update_vcs to use other commands, ala hg or svn
- Move vcs related commands to a plugin
- Allow async builds
